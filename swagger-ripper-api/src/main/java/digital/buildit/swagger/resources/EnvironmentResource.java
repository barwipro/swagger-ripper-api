package digital.buildit.swagger.resources;

import digital.buildit.swagger.domain.Swagger;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.apache.cxf.common.i18n.Exception;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.io.IOException;
import java.util.Collection;

@Path("/environment")
@Api("/environment")
public interface EnvironmentResource {

    @GET
    @Path(value = "/")
    @Produces(MediaType.APPLICATION_JSON)
    @ApiOperation("Get the list of all the environments")
    Collection<String> getAll();

    @DELETE
    @Path(value = "/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation("Delete environment")
    void delete(@PathParam("id") String id);

    @DELETE
    @Path(value = "/")
    @Produces(MediaType.TEXT_PLAIN)
    @ApiOperation("Delete all environments")
    void delete();

    @POST
    @Path("/")
    @Consumes(MediaType.TEXT_PLAIN)
    void save(String swaggerSpec) throws Exception, IOException;

}
