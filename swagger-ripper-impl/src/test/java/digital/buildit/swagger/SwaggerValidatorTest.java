package digital.buildit.swagger;

import digital.buildit.swagger.utilities.ReadFromResources;
import digital.buildit.swagger.utilities.SwaggerValidator;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;

/**
 * Created by bartosz on 21/02/2017.
 */
public class SwaggerValidatorTest {
    private SwaggerValidator swaggerValidator;

    private String getResourcePath(String path) throws UnsupportedEncodingException {
        return URLDecoder.decode(new ReadFromResources().getClass().getClassLoader().getResource(path).toString().replace("file:", ""), "UTF8");
    }

    private String getFile(String path) throws IOException {
        String url = getResourcePath(path);
        FileInputStream in = new FileInputStream(new File(url));
        return IOUtils.toString(in);
    }

    @Before
    public void setUp() throws Exception {
        swaggerValidator = new SwaggerValidator();
    }

    @Test
    public void givenInvalidFileType_returnsFalse() throws IOException {
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some.txt"), false);
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some."), false);
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some"), false);
    }

    @Test
    public void givenValidFileType_returnsTrue() throws IOException {
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some.json"), true);
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some.yaml"), true);
        Assert.assertEquals(swaggerValidator.isSwaggerFile("some.yml"), true);
    }


    @Test
    public void givenEmptySwagger_returnsFalse() throws IOException {
        String swagger = getFile("empty.txt");

        Assert.assertEquals(swaggerValidator.isValid(swagger), false);
        Assert.assertEquals(swaggerValidator.isValid(null), false);
    }

    @Test
    public void givenInvalidSwagger_returnsFalse() throws IOException {
        String swagger = getFile("invalid.json");

        Assert.assertEquals(swaggerValidator.isValid(swagger), false);
    }

    @Test
    public void givenValidSwagger_returnsTrue() throws IOException {
        String swaggerJson = getFile("products.json");
        String swaggerYaml = getFile("uber.yaml");

        Assert.assertEquals(swaggerValidator.isValid(swaggerJson), true);
        Assert.assertEquals(swaggerValidator.isValid(swaggerYaml), true);
    }
}
