package digital.buildit.swagger;

import digital.buildit.swagger.utilities.ReadFromResources;
import digital.buildit.swagger.utilities.SwaggerExtractor;
import digital.buildit.swagger.utilities.SwaggerValidator;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;
import org.mockito.stubbing.Answer;

import java.io.*;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static org.mockito.Mockito.mock;

public class SwaggerExtractorTest {

    private SwaggerExtractor swaggerExtractor;

    private SwaggerValidator swaggerValidator;

    private String getResourcePath(String path) throws UnsupportedEncodingException {
        return URLDecoder.decode(new ReadFromResources().getClass().getClassLoader().getResource(path).toString().replace("file:", ""), "UTF8");
    }

    private String getFile(String path) throws IOException {
        String url = getResourcePath(path);
        FileInputStream in = new FileInputStream(new File(url));
        return IOUtils.toString(in);
    }

    private InputStream zipFileWithFiles(List<String> paths) throws IOException {
        File file = File.createTempFile("swagger-", ".tmp");
        file.deleteOnExit();

        ZipOutputStream out = new ZipOutputStream(new FileOutputStream(file));
        paths.forEach((String path) -> {
            ZipEntry e = new ZipEntry(path);
            try {
                out.putNextEntry(e);
                byte[] data = IOUtils.toByteArray(new FileInputStream(new File(path)));
                out.write(data, 0, data.length);
                out.closeEntry();
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        });

        out.close();
        return new FileInputStream(file);
    }

    @Before
    public void setUp() throws Exception {
        swaggerValidator = mock(SwaggerValidator.class, (Answer) invocationOnMock -> true);
        swaggerExtractor = new SwaggerExtractor(swaggerValidator);
    }

    @Test
    public void givenJarWithoutSwagger_returnsEmpty() throws IOException {
        List<String> files = new ArrayList<>();
        InputStream zipFile = zipFileWithFiles(files);

        Assert.assertNull(swaggerExtractor.extractSwagger(zipFile));
    }

    @Test
    public void givenJarWithSwagger_returnsSwagger() throws IOException {
        String url = URLDecoder.decode(new ReadFromResources().getClass().getClassLoader().getResource("uber.jar").toString().replace("file:", ""), "UTF8");
        FileInputStream in = new FileInputStream(new File(url));

        Assert.assertNotNull(swaggerExtractor.extractSwagger(in));
    }

    @Test
    public void givenJarWitSwagger_returnsSwagger() throws IOException {
        List<String> files = new ArrayList<>();
        files.add(getResourcePath("petstore.json"));
        InputStream zipFile = zipFileWithFiles(files);

        String result = getFile("petstore.json");
        Assert.assertEquals(swaggerExtractor.extractSwagger(zipFile), result);
    }
}