package digital.buildit.swagger.resources;

import digital.buildit.swagger.domain.Swagger;
import digital.buildit.swagger.repository.EnvironmentRepository;
import digital.buildit.swagger.repository.SwaggerRepository;
import digital.buildit.swagger.utilities.SwaggerDownloader;
import digital.buildit.swagger.utilities.SwaggerExtractor;
import org.apache.cxf.common.i18n.Exception;
import org.apache.cxf.jaxrs.ext.multipart.Attachment;
import org.apache.cxf.jaxrs.ext.multipart.Multipart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collection;

@Service
public class EnvironmentResourceImpl implements EnvironmentResource {

    @Autowired
    private EnvironmentRepository repository;

    public Collection<String> getAll() {
        return repository.findAll();
    }

    public void delete(String id) {
        repository.delete(id);
    }

    public void delete() {
        repository.purge();
    }

    public void save(String environmentUrl) throws Exception, IOException {
        repository.save(environmentUrl);
    }
}
