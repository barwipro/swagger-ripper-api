package digital.buildit.swagger.resources;

import digital.buildit.swagger.utilities.ReadFromResources;
import org.springframework.stereotype.Service;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.net.URLDecoder;

@Service
@Path("/web")
public class StaticContentHandler
{
    private File getFile(String path) throws IOException {
        URL url = new ReadFromResources().getClass().getClassLoader().getResource(path);
        return new File(URLDecoder.decode(url.toString().replace("file:", ""), "UTF8"));
    }

    @GET
    @Path("{docPath:.*}")
    public Response getFolder(@PathParam("docPath") String docPath) throws IOException {
        File file = null;
        if ("".equals(docPath) || "/".equals(docPath))
        {
            file = getFile("web/index.html");
        }
        else
        {
            file = getFile("web/" + docPath);
        }
        return Response.ok(file).build();
    }

}