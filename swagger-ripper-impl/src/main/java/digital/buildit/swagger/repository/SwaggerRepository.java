package digital.buildit.swagger.repository;


import digital.buildit.swagger.domain.Swagger;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public interface SwaggerRepository {
    Map<String, Swagger> environments = new LinkedHashMap<String, Swagger>();

    Collection<Swagger> findAll();

    String findById(String id);

    void delete(String id);

    Swagger save(Swagger swagger);

    Swagger save(String swagger);

    void purge();
}
