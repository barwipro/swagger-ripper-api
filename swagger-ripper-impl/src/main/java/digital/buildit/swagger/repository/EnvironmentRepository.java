package digital.buildit.swagger.repository;

import digital.buildit.swagger.domain.Swagger;

import java.util.Collection;

/**
 * Created by bartosz on 16/02/2017.
 */
public interface EnvironmentRepository {
    Collection<String> findAll();

    void delete(String id);

    String save(String swagger);

    void purge();
}
