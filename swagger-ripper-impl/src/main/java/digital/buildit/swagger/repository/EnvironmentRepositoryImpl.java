package digital.buildit.swagger.repository;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectWriter;
import digital.buildit.swagger.domain.Swagger;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.io.IOException;
import java.util.*;

@Repository
public class EnvironmentRepositoryImpl implements EnvironmentRepository{
    Map<String, String> environments = new LinkedHashMap<String, String>();

    public EnvironmentRepositoryImpl(){

    }

    public Collection<String> findAll() {
        return environments.values();
    }

    public String findById(String id) {
        return environments.get(id);
    }

    public void delete(String id) {
        environments.remove(id);
    }

    public String save(String environmentUrl) {
        environments.put(generateID(), environmentUrl);
        return environmentUrl;
    }

    public void purge() {
        environments = new TreeMap<String, String>();
    }

    private String generateID(){
        return UUID.randomUUID().toString();
    }
}
