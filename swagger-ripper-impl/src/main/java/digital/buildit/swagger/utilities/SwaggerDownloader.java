package digital.buildit.swagger.utilities;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

@Component
public class SwaggerDownloader {

    @Autowired
    private SwaggerExtractor swaggerExtractor;

    public String downloadSwagger(String swaggerUrl) throws IOException {
        URL url = new URL(swaggerUrl);
        if(swaggerUrl.endsWith(".jar"))
        {
            return swaggerExtractor.extractSwagger(url.openStream());
        }
        else
        {
            InputStreamReader inputStreamReader = new InputStreamReader(url.openStream());
            return IOUtils.toString(inputStreamReader);
        }
    }

}
