package digital.buildit.swagger.utilities;

import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import org.apache.commons.io.IOUtils;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * Created by bartosz on 21/02/2017.
 */
@Component
public class SwaggerValidator {
    private static class SwaggerSpec {
        private String swagger;

        public SwaggerSpec(){
        }

        public void setSwagger(String value){
            this.swagger = value;
        }

        public String getSwagger(){
            return this.swagger;
        }
    }

    public boolean isValid(String swagger)  {
        if(swagger == null || swagger.equals("")){
            return false;
        }
        if(isSwagger(swagger, new JsonFactory()) == false && isSwagger(swagger, new YAMLFactory()) == false){
            return false;
        }
        return true;
    }

    private boolean isSwagger(String swagger, JsonFactory factory) {
        SwaggerSpec swaggerSpec;
        try {
            ObjectMapper objectMapper = new ObjectMapper(factory);
            objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            swaggerSpec = objectMapper.readValue(IOUtils.toInputStream(swagger), SwaggerSpec.class);
        } catch (IOException e) {
            return false;
        }
        return swaggerSpec.getSwagger() != null;
    }

    public boolean isSwaggerFile(String fileName) {
        return fileName.endsWith(".json") || fileName.endsWith(".yml") || fileName.endsWith(".yaml");
    }
}
