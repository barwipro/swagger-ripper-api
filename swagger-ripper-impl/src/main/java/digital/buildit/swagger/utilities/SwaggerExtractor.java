package digital.buildit.swagger.utilities;

import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.*;
import java.nio.file.Files;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

@Component
public class SwaggerExtractor {

    private SwaggerValidator swaggerValidator;

    @Autowired
    public SwaggerExtractor(SwaggerValidator swaggerValidator){
        this.swaggerValidator = swaggerValidator;
    }

    public String extractSwagger(InputStream in) throws IOException {

        File temp = createTempFile(in);

        ZipFile zip = new ZipFile(temp);

        for (Enumeration e = zip.entries(); e.hasMoreElements(); ) {
            ZipEntry entry = (ZipEntry) e.nextElement();

            InputStream inputStream = zip.getInputStream(entry);
            InputStreamReader inputStreamReader = new InputStreamReader(inputStream);

            String content = null;
            if(swaggerValidator.isSwaggerFile(entry.getName()) && (content = getContents(inputStreamReader)) != null && swaggerValidator.isValid(content)){
                temp.delete();
                return content;
            }
        }
        return null;
    }

    private File createTempFile(InputStream in) throws IOException {
        File temp = File.createTempFile("swagger-", ".tmp");
        temp.deleteOnExit();

        IOUtils.copy(in, new FileOutputStream(temp));
        return temp;
    }

    private String getContents(InputStreamReader inputStreamReader) throws IOException {
        return IOUtils.toString( inputStreamReader );
    }
}
